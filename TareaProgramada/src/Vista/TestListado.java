/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Modelo.TareaProgramada;
import Modelo.ListaTarea;
/**
 *
 * @author Lab05pc16
 */
public class TestListado {
   public static void main(String[] args) {
         
        ListaTarea listatareas = new ListaTarea();
        
//        Adicionamos a los listados

         TareaProgramada uno = new TareaProgramada(001, 4, 2.1, 201181710);
         TareaProgramada dos = new TareaProgramada(001, 4, 2.1, 201181710);
         TareaProgramada tres = new TareaProgramada(002, 9, 1.5, 200110710);
         TareaProgramada cuatro = new TareaProgramada(004, 5, 0.51, 201211710);
         TareaProgramada cinco = new TareaProgramada(004, 5, 0.51, 201211710);
   
    
//    Insertamos

    listatareas.insertar(uno);
    listatareas.insertar(dos);
    listatareas.insertar(tres);
    listatareas.insertar(cuatro);
    listatareas.insertar(cinco);
    
        System.out.println(listatareas.toString());
        System.out.println("//////////////////////////////////////////////////");
        
//        Probando metodo de listado

        System.out.println("Menor segun HASH: "+"\n"+listatareas.getMenor());
        System.out.println("//////////////////////////////////////////////////");
        
        System.out.println("De menor a mayor segun HASH: "+"\n"+listatareas.getSort());
        System.out.println("//////////////////////////////////////////////////");
        
        System.out.println("Menores segun HASH: "+"\n"+listatareas.getMenores());
        System.out.println("//////////////////////////////////////////////////");
        
        
        System.out.println("Sin Repetidos: "+"\n"+listatareas.toString());
        System.out.println("//////////////////////////////////////////////////");
        
        System.out.println("Union de Listas"+"\n"+listatareas.getUnion(listatareas.getSort(),listatareas.getSort()));
        System.out.println("//////////////////////////////////////////////////");
        
        System.out.println("Interseccion de Listas"+"\n"+listatareas.getInterseccion(listatareas.getSort(),listatareas.getSort()));
        System.out.println("//////////////////////////////////////////////////");
        
        System.out.println("Diferencia de Listas"+"\n"+listatareas.getDiferencia(listatareas.getSort(),listatareas.getSort()));
    }
    
    
}
