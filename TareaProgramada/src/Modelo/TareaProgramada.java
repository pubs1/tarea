/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Lab05pc16
 */
public class TareaProgramada implements Comparable<TareaProgramada>{
    
    private int IDTarea;
    private int Frecuencia;
    private double Duracion;
    private int ultimoEjecucion;

    public TareaProgramada() {
    }

    public TareaProgramada(int IDTarea, int Frecuencia, double Duracion, int ultimoEjecucion) {
        this.IDTarea = IDTarea;
        this.Frecuencia = Frecuencia;
        this.Duracion = Duracion;
        this.ultimoEjecucion = ultimoEjecucion;
    }

    public int getIDTarea() {
        return IDTarea;
    }

    public void setIDTarea(int IDTarea) {
        this.IDTarea = IDTarea;
    }

    public int getFrecuencia() {
        return Frecuencia;
    }

    public void setFrecuencia(int Frecuencia) {
        this.Frecuencia = Frecuencia;
    }

    public double getDuracion() {
        return Duracion;
    }

    public void setDuracion(double Duracion) {
        this.Duracion = Duracion;
    }

    public int getUltimoEjecucion() {
        return ultimoEjecucion;
    }

    public void setUltimoEjecucion(int ultimoEjecucion) {
        this.ultimoEjecucion = ultimoEjecucion;
    }

    @Override
    public String toString() {
        return "TareaProgramada [" + "IDTarea: " + IDTarea + ", Frecuencia: " + Frecuencia + ", Duracion: " + Duracion + ", ultimoEjecucion: " + ultimoEjecucion + ", Hash: "+this.getHash()+']' ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.IDTarea;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TareaProgramada other = (TareaProgramada) obj;
        double valorMio = this.getHash();     
        double valorOther = other.getHash();

        if (valorMio == valorOther) {
            return true;

        } else {
            return false;
        }
    }
    
    @Override
    public int compareTo(TareaProgramada other) {

        double miTarea = this.getHash();
        double otroTarea = other.getHash();
        double restaTareas = miTarea - otroTarea;
        
        if (restaTareas == 0) {
            return 0;
        } else if (restaTareas < 0) {
            return -1;
        } else {
            return 1;
        }
    }
    
    /*Idea de implementacion con numeros primos de https://stackoverflow.com/questions/38280843/how-to-pick-prime-numbers-to-calculate-the-hash-code
    
    El hash creado para una clase TareaProgramada que tiene como atributos
    un ID, una Frecuencia, una duracion y una ultima fecha de ejecucion, se construyo:
    
    Multiplicando un numero primo seleccionado por cada valor y sumando todos los valores.
    */
    
    public int getHash(){
        
        int primo = 31;
        int resultado;
        
        resultado = primo * this.IDTarea+primo*this.getEntero()+primo*this.Frecuencia+primo*this.ultimoEjecucion;
        
        return resultado;
    }
    
    //Metodo para convertir la variable double Duracion en un valor unico entero (Ineficiente para numeros con demasiadas decimales, max it 200)
    
    public int getEntero(){
        
        double suma = this.Duracion;
        int it = 0;
        
        while (suma % 1 != 0 && it < 200) {
            suma += this.Duracion;
            it++;
        }
        
        int numeroEntero = (int) suma;
        
        return numeroEntero;
        
    }
    
}
