/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
import java.util.LinkedList;
/**
 *
 * @author Lab05pc16
 */
public class ListaTarea {
    
    private LinkedList <TareaProgramada> myLista = new LinkedList();

    public ListaTarea() {
    }

    public LinkedList<TareaProgramada> getMyLista() {
        return myLista;
    }

    public void setMyLista(LinkedList<TareaProgramada> myLista) {
        this.myLista = myLista;
    }

    @Override
    public String toString() {
       String msg= "";
        
        for (TareaProgramada tareaprogramada : myLista) {
            
            msg+=tareaprogramada.toString()+"\n";
        }
        return msg;
    }
    
    public void insertar(TareaProgramada nuevo){
        this.myLista.add(nuevo);
    }
    
    public void borrar(TareaProgramada valor){
        this.myLista.remove(valor);
    }
    
    public TareaProgramada getMenor(){
        TareaProgramada menor = this.myLista.get(0);
        
        for (int i = 1; i < this.myLista.size(); i++) {
            TareaProgramada dos = this.myLista.get(i);
            int c = menor.compareTo(dos);
            
            if (c > 0) {
                menor = dos;
            }
        }
        
        return menor;
    }
    
//    Ordena la lista usando el método de ordenamiento burbuja. 
    public ListaTarea getSort(){
        
        ListaTarea ordenados = new ListaTarea();
        for (int i = 0; i < this.myLista.size() - 1; i++) {
            for (int j = 0; j < this.myLista.size() - i - 1; j++) {
                if (this.myLista.get(j).compareTo(this.myLista.get(j + 1)) > 0) {
                    TareaProgramada temp = this.myLista.get(j);
                    this.myLista.set(j, this.myLista.get(j + 1));
                    this.myLista.set(j + 1, temp);
                }
            }   
        }

        for (int i = 0; i < this.myLista.size(); i++) {
            ordenados.insertar(this.myLista.get(i));
        }
    
        return ordenados;
    }
    
    public ListaTarea getMenores(){
        
        ListaTarea menores = new ListaTarea();
        
            for (int i = 0; i < this.myLista.size(); i++) {
                TareaProgramada nuevo = this.myLista.get(i);
                if (this.myLista.get(i).compareTo(getMenor()) == 0) {
                    menores.insertar(nuevo);
                }   
            }          
        return menores;
    }
    
    public void borrarRepetidos(){
        for (int i = 0; i < this.myLista.size()-1; i++) {
            for (int j = i+1; j < this.myLista.size(); j++) {
                if (this.myLista.get(i).equals(this.myLista.get(j))) {
                    this.myLista.remove(i);
                    }
                }
            }
        
        }
    
    public ListaTarea getUnion(ListaTarea uno, ListaTarea dos) {
    ListaTarea nueva = new ListaTarea();

        for (int i = 0; i < uno.getMyLista().size(); i++) {
            nueva.insertar(uno.getMyLista().get(i)); 
        }

        for (int i = 0; i < dos.getMyLista().size(); i++) {
            nueva.insertar(dos.getMyLista().get(i)); 
        }

        return nueva;
    }
    
    public ListaTarea getInterseccion(ListaTarea uno, ListaTarea dos){
        ListaTarea nueva = new ListaTarea();
        
        for (int i = 0; i < uno.getMyLista().size(); i++) {
            TareaProgramada nuevo = uno.getMyLista().get(i);
            if (dos.getMyLista().contains(nuevo)) {
                nueva.insertar(nuevo);
            }
        }
        return nueva;
    }
    
    public ListaTarea getDiferencia(ListaTarea uno, ListaTarea dos){
        ListaTarea nueva = new ListaTarea();
        
        for (int i = 0; i < uno.getMyLista().size(); i++) {
            TareaProgramada nuevo = uno.getMyLista().get(i);
            if (!dos.getMyLista().contains(nuevo)) {
                nueva.insertar(nuevo);
            }
        }
        return nueva;
    }
  
}
